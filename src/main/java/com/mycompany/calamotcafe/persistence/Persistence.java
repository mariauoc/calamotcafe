package com.mycompany.calamotcafe.persistence;

import com.mycompany.calamotcafe.model.CoffeShop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author mfontana
 */
public class Persistence {

    private String folder;
    private String fileName;
    private String pathFile;
    private File folderFile;

    public Persistence() {
        folder = "dades";
        fileName = "coffes.txt";
        pathFile = folder + File.separator + fileName;
        folderFile = new File(folder);
    }

    public void writeCoffe(CoffeShop c) {
        if (!folderFile.exists()) {
            folderFile.mkdir();
        }
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(pathFile, true));
            bw.write(c.toString());
            bw.newLine();
            bw.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void readCoffe() {
        // Ejemplo de lectura pero falta implementar que lea esas líneas y las 
        // cargue en un ArrayList<CoffeShop> 
        if (!folderFile.exists()) {
            // aquí devolverías el arrayList de coffeshop vacío, ya que si no existe la carpeta no hay datos por leer
        } else {
            File f = new File(pathFile);
            if (!f.exists()) {
                // aquí devolverías el arrayList de coffeshop vacío, ya que si no existe la carpeta no hay datos por leer
            } else {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(f));
                    String linea;
                    while ((linea = br.readLine()) != null) {
                        // Aquí en realidad leeríais la línea y harías split, etc para crear los objetos y añadirlos al ArrayList que retornariais
                        System.out.println(linea);
                    }
                    br.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

}
