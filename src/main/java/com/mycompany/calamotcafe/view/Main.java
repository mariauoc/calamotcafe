package com.mycompany.calamotcafe.view;

import com.mycompany.calamotcafe.model.CoffeMachine;
import com.mycompany.calamotcafe.model.CoffeShop;
import com.mycompany.calamotcafe.persistence.Persistence;
import java.util.ArrayList;

/**
 *
 * @author mfontana
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // HashMap<String, CoffeShop>
        ArrayList<CoffeShop> cafeterias = new ArrayList<>();
        AskData askData = new AskData();
        int opcion;
        testFichero();
        do {
            showMenu();
            opcion = askData.askInt("Escoge una opción:");
            switch (opcion) {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    System.out.println("Hasta la próxima");
                    break;
                default:
                    System.out.println("Opción incorrecta");
        }
        } while (opcion !=5);
    }
    
    public static void testFichero() {
        CoffeShop c = new CoffeShop("Calamot Cafe", "Gava");
        Persistence persistence = new Persistence();
        persistence.writeCoffe(c);
        persistence.readCoffe();
    }
    
    public static void showMenu() {
        System.out.println("Cafeterías La Pera Limonera");
        System.out.println("1. Registrar cafetería");
        System.out.println("2. Registrar cafetera");
        System.out.println("3. Trabajar en una cafetería.");
        System.out.println("4. Mostrar cafeterías");
        System.out.println("5. Salir");
    }
    
}
